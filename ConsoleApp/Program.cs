﻿using ImageLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginStuff
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(">>> Creating ImageProcessor and loading plugins ...");

            var processor = new ImageProcessor();

            // Load plugins from a separate assembly using reflection.
            processor.LoadPlugins(AppDomain.CurrentDomain.BaseDirectory + "/SampleImagePlugins.dll");

            var a = processor
                .NewPipeline()
                .AddOp("blur", "{ radius: 5 }")
                .AddOp("blur", "{ radius: 10 }")
                .AddOp("blur", "{ radius: 15 }");
            var b = processor
                .NewPipeline()
                .AddOp("resize", "{ width: 20, height: 20 }")
                .AddOp("blur", "{ radius: 10 }");

            Console.WriteLine(">>> Running Pipline A ...");
            a.ExecPipeline(img: null);

            Console.WriteLine(">>> Running Pipline B ...");
            b.ExecPipeline(img: null);

            // Pipelines can be run in parallel with built-in TPL.

            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
