﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ImageLib
{
    public class ImageProcessor
    {
        private Dictionary<string, Type> _pluginMap = new Dictionary<string, Type>();

        /// <summary>
        /// Returns a builder for new image processing pipeline.
        /// </summary>
        public Pipeline NewPipeline()
        {
            return new Pipeline(_pluginMap);
        }

        /// <summary>
        /// Loads image plugins from assembly.
        /// </summary>
        public List<Type> LoadPlugins(Assembly asm)
        {
            Console.WriteLine($"Loading image plugins from {asm.FullName} ...");

            var result = new List<Type>();
            var imageOpType = typeof(IImageOp);
            var types = asm.GetTypes()
                .Where(x => imageOpType.IsAssignableFrom(x));

            foreach (var type in types)
            {
                // XXX: name clashes might occur between plugins in different namespaces
                // or even assemblies but we don't care about such intricacies atm.
                var name = GetOpNameFromType(type);
                if (!_pluginMap.ContainsKey(name))
                {
                    Console.WriteLine($"Loaded {type.FullName} from {asm.FullName}");
                    _pluginMap.Add(name, type);
                    result.Add(type);
                }
            }

            return result;
        }

        /// <summary>
        /// Loads image plugins from assembly located in <paramref name="dllpath"/>.
        /// </summary>
        public IEnumerable<Type> LoadPlugins(string dllpath)
        {
            var asm = Assembly.LoadFile(dllpath);
            return LoadPlugins(asm).ToList();
        }


        private string GetOpNameFromType(Type type)
        {
            var norm = type.Name.ToLowerInvariant();
            return norm.EndsWith("op") && norm.Length > 2 ? norm.Substring(0, norm.Length - 2) : norm;
        }
    }
}
