﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageLib
{
    public class Pipeline
    {
        private Dictionary<string, Type> _pluginMap;
        private List<IImageOp> _opList = new List<IImageOp>();

        public Pipeline(Dictionary<string, Type> pluginMap)
        {
            _pluginMap = pluginMap;
        }

        /// <summary>
        /// Adds image op to pipeline (for building a pipeline from user supplied data).
        /// </summary>
        /// <param name="opName">Image operation name</param>
        /// <param name="json">JSON data containing IImageOp params</param>
        public Pipeline AddOp(string opName, string json)
        {
            if (!_pluginMap.TryGetValue(opName, out var type))
                throw new ArgumentException(nameof(opName));

            var op = (IImageOp)JsonConvert.DeserializeObject(json, type);
            _opList.Add(op);
            return this;
        }

        /// <summary>
        /// Adds image op to pipeline (for programmatic use).
        /// </summary>
        public Pipeline AddOp(IImageOp op)
        {
            _opList.Add(op);
            return this;
        }

        public Image ExecPipeline(Image img)
        {
            // TODO: shouldn't mutate input image. Do this stuff on a copy instead.
            foreach (var op in _opList)
                op.Apply(img);
            return img;
        }
    }
}
