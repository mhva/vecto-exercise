﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ImageLib
{
    /// <summary>
    /// Represents a singular image operation.
    /// </summary>
    public interface IImageOp
    {
        /// <summary>
        /// Applies op to pixel data.
        /// </summary>
        void Apply(Image img);
    }
}
