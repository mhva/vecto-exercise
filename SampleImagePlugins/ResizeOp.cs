﻿using ImageLib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleImagePlugins
{
    public class ResizeOp : IImageOp
    {
        public int? Width { get; set; }
        public int? Height { get; set; }

        public void Apply(Image img)
        {
            Console.WriteLine($"RESIZE: {Width ?? img.Width}x{Height ?? img.Height}");
        }
    }
}
