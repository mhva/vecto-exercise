﻿using ImageLib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleImagePlugins
{
    public class BlurOp : IImageOp
    {
        public int Radius { get; set; }

        public void Apply(Image img)
        {
            Console.WriteLine($"BLUR: {Radius} px");
        }
    }
}
